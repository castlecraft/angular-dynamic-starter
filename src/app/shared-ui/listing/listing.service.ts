import { Injectable } from '@angular/core';
import { StorageService } from '../../common/services/storage/storage.service';
import { ACCESS_TOKEN, API_PREFIX, APP_URL } from '../../constants/storage';
import { HttpClient, HttpParams } from '@angular/common/http';
import {
  HandleError,
  HttpErrorHandler,
} from '../../common/services/http-error-handler/http-error-handler.service';
import { AUTHORIZATION } from '../../constants/common';

@Injectable({
  providedIn: 'root',
})
export class ListingService {
  handleError: HandleError;

  constructor(
    private storageService: StorageService,
    httpErrorHandler: HttpErrorHandler,
    private http: HttpClient,
  ) {
    this.handleError = httpErrorHandler.createHandleError('ListingService');
  }

  findModels(
    model: string,
    filter = '',
    sortOrder = 'asc',
    pageNumber = 0,
    pageSize = 10,
  ) {
    const baseUrl = this.storageService.getInfo(APP_URL);
    const url = `${baseUrl}/${API_PREFIX}/${model}/v1/list`;
    const params = new HttpParams()
      .set('limit', pageSize.toString())
      .set('offset', (pageNumber * pageSize).toString())
      .set('search', filter)
      .set('sort', sortOrder);
    return this.http.get(url, { params, headers: this.getHeaders() });
  }

  getHeaders() {
    return {
      [AUTHORIZATION]: `Bearer ${localStorage.getItem(ACCESS_TOKEN)}`,
    };
  }
}
