import { ValidatorFn } from '@angular/forms';

export interface FieldConfig {
  disabled?: boolean;
  label?: string;
  name: string;
  options?: string[];
  placeholder?: string;
  type: string;
  validation?: ValidatorFn[];
  value?: any;
  click?: any;
  width?: number;
  color?: string;
}

export interface StandardContainer {
  content?: FieldConfig[] | any[];
  footer?: FieldConfig[];
}
