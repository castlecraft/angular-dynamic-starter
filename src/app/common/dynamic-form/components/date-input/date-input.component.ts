import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { Field } from '../../models/field.interface';
import { FieldConfig } from '../../models/field-config.interface';

@Component({
  selector: 'date-input',
  styleUrls: ['date-input.component.scss'],
  template: `
    <mat-form-field
      [style.width.%]="config.width || 100"
      [formGroup]="group"
      appearance="outline"
    >
      <mat-label>{{ config.label }}</mat-label>

      <input
        matInput
        [matDatepicker]="i"
        [formControlName]="config.name"
        [placeholder]="config.placeholder"
      />

      <mat-datepicker-toggle matSuffix [for]="i"></mat-datepicker-toggle>

      <mat-datepicker #i> </mat-datepicker>
    </mat-form-field>
  `,
})
export class DateInputComponent implements Field {
  config: FieldConfig;
  group: FormGroup;
}
