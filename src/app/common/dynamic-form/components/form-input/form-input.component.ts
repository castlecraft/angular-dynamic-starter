import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { Field } from '../../models/field.interface';
import { FieldConfig } from '../../models/field-config.interface';

@Component({
  selector: 'form-input',
  styleUrls: ['form-input.component.scss'],
  template: `
    <mat-form-field
      [style.width.%]="config.width || 100"
      class="dynamic-field form-input"
      [formGroup]="group"
      appearance="outline"
    >
      <mat-label>{{ config.label }}</mat-label>

      <input matInput [formControlName]="config.name" />
    </mat-form-field>
  `,
})
export class FormInputComponent implements Field {
  config: FieldConfig;
  group: FormGroup;
}
